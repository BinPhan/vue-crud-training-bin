export const POSITION = [
    {
        id: 1,
        name: "Việt Nam"
    },
    {
        id: 2,
        name: "Châu Á"
    },
    {
        id: 3,
        name: "Châu Âu"
    },
    {
        id: 4,
        name: "Châu Mĩ"
    }
]

export const CATEGORY = [
    {
        id: 0,
        name: 'Thời sự'
    },
    {
        id: 1,
        name: 'Thế giới'
    },
    {
        id: 2,
        name: 'Kinh doanh'
    },
    {
        id: 3,
        name: 'Giải trí'
    },
    {
        id: 4,
        name: 'Thể thao'
    },
    {
        id: 5,
        name: 'Pháp luật'
    },
    {
        id: 6,
        name: 'Khoa học'
    },
    {
        id: 7,
        name: 'Công nghệ'
    },
    {
        id: 8,
        name: 'Đời sống'
    },
    {
        id: 9,
        name: 'Tin tức'
    },
    {
        id: 10,
        name: 'Giáo dục'
    },
    {
        id: 11,
        name: 'Kinh tế'
    },
    {
        id: 12,
        name: 'Địa phương'
    },
]

